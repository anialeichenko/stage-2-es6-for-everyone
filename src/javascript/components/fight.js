import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const leftFighterIndicator = document.getElementById('left-fighter-indicator');
    const rightFighterIndicator = document.getElementById('right-fighter-indicator');

    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    
    let firstFighterCanCriticalHit = true;
    let secondFighterCanCriticalHit = true;

    const pressedKeysSet = new Set();

    const keydownHandler = (event) => {
      pressedKeysSet.add(event.code);

      if(event.code === controls.PlayerOneAttack) {
        if(!pressedKeysSet.has(controls.PlayerOneBlock) && !pressedKeysSet.has(controls.PlayerTwoBlock)) {
          secondFighterHealth -= getDamage(firstFighter, secondFighter);
        }
      }

      if(event.code === controls.PlayerTwoAttack) {
        if(!pressedKeysSet.has(controls.PlayerOneBlock) && !pressedKeysSet.has(controls.PlayerTwoBlock)) {
          firstFighterHealth -= getDamage(secondFighter, firstFighter);
        }
      }

      if (firstFighterCanCriticalHit && controls.PlayerOneCriticalHitCombination.every((key) => pressedKeysSet.has(key))) {
        firstFighterCanCriticalHit = false;
        
        setTimeout(() => {
          firstFighterCanCriticalHit = true;
        }, 10000);

        secondFighterHealth -= firstFighter.attack * 2;
      }

      if (secondFighterCanCriticalHit && controls.PlayerTwoCriticalHitCombination.every((key) => pressedKeysSet.has(key))) {
        secondFighterCanCriticalHit = false;
        
        setTimeout(() => {
          secondFighterCanCriticalHit = true;
        }, 10000);

        firstFighterHealth -= secondFighter.attack * 2;
      }
      
      if(secondFighterHealth <= 0 || firstFighterHealth <= 0) {
        if(firstFighterHealth <= 0) {
          firstFighterHealth = 0;
          resolve(secondFighter);
        }
        if(secondFighterHealth <= 0) {
          secondFighterHealth = 0;
          resolve(firstFighter);
        }
        document.removeEventListener('keydown', keydownHandler);
        document.removeEventListener('keyup', keyupHandler);
        
      }
      rightFighterIndicator.style.width = `${100 * secondFighterHealth / secondFighter.health}%`;
      leftFighterIndicator.style.width = `${100 * firstFighterHealth / firstFighter.health}%`;
    }


    const keyupHandler = (event) => {
      pressedKeysSet.delete(event.code);
    }

    document.addEventListener('keydown', keydownHandler);
    document.addEventListener('keyup', keyupHandler);
  });
}

export function getDamage(attacker, defender) {
   const damage = getHitPower(attacker) - getBlockPower(defender);
   return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}
