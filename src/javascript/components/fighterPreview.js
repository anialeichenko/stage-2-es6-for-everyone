import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const nameElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___name',
    });
    nameElement.textContent = fighter.name;
    fighterElement.append(nameElement);
    const imgElement = createFighterImage(fighter, position);
    fighterElement.append(imgElement);
    const healthElement = createElement({
      tagName: 'div',
    });
    healthElement.textContent = "Health: " + fighter.health;
    fighterElement.append(healthElement);
    const defenseElement = createElement({
      tagName: 'div',
    });
    defenseElement.textContent = "Defense: " + fighter.defense;
    fighterElement.append(defenseElement);
    const attackElement = createElement({
      tagName: 'div',
    });
    attackElement.textContent = "Attack: " + fighter.attack;
    fighterElement.append(attackElement);
  }
  return fighterElement;
}

export function createFighterImage(fighter, position) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: `fighter-preview___img fighter-preview___img--${position}`,
    attributes,
  });

  return imgElement;
}
