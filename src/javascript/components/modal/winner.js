import { showModal } from './modal';
import { createFighters } from '../../components/fightersView';
import { fighterService } from '../../services/fightersService';

export function showWinnerModal(fighter) {
  console.log(fighter);
  showModal({ 
    title: `${fighter.name} win!`,
    bodyElement: '',
    onClose: async () => {
      const rootElement = document.getElementById('root');
      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);
      rootElement.innerHTML = '';
      rootElement.appendChild(fightersElement);
    }
  });
  // call showModal function 
}
